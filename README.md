<a name="module_lahuanjs-com-events"></a>
#lahuanjs-com-events
Component version of a Node.js-like EventEmitter for use with `lahuanjs-ent`.

Uses (and bundles) the standalone [`events`](https://www.npmjs.org/package/events) module.

[![Build](http://img.shields.io/travis/lahuan/lahuanjs-com-events.svg?style=flat)](https://travis-ci.org/lahuan/lahuanjs-com-events)
[![Dependencies](http://img.shields.io/david/lahuan/lahuanjs-com-events.svg?style=flat)](https://david-dm.org/lahuan/lahuanjs-com-events)
[![License](http://img.shields.io/badge/license-mit-green.svg?style=flat)](https://github.com/lahuan/lahuanjs-com-events/blob/master/LICENSE)
[![Release](http://img.shields.io/badge/release-v0.1.0-orange.svg?style=flat)](https://github.com/lahuan/lahuanjs-com-events/releases)

##Usage

Basic usage:

```js

//Create an entity with this component.
var ent = new Ent();
ent.add( events );

// Add a listener.
var listener = function( payload ) {
    console.log( "Listened to "+payload+"." );
}
ent.events.on( 'test', listener );

// Emit event.
ent.events.emit( 'test', "an entity" );
// "Listened to an entity."

```

Usage in the browser via UMD build:

```html
<script src="<path>/lahuanjs-com-events.js"></script>
<script>
    var events = lahuanjs.com.events;
    //...
</script>
```

##API
<a name="exp_module_lahuanjs-com-events"></a>
###events() ⏏
Component version of a Node.js-like EventEmitter.

Complies to the [`ComLike`](https://github.com/lahuan/lahuanjs-ent#module_lahuanjs-ent..ComLike) specification.

It is recommended to Use the original class directly for any general-purpose inheritance or extension, if available. 
Of course, this constructor _can_ be inherited to form new components or classes, or instantiated as is. In which case the only extension to the original class -- the special `comlike` destruction method -- will also be available.

Refer to the [Node.js Events documentation](http://nodejs.org/api/events.html) for additional usage information.

####Type

`ComLike`  


