var test = require( 'tape' ).test;
var events = require( '../' );
var Ent = require( 'lahuanjs-ent' );
var EventEmitter = require('events').EventEmitter;

test("Com tests.", function (t) {
    
    // Create entity with events component.
    var ent = new Ent();
    ent.add( events );
    
    
    
    // See if comlike is a valid instance of the base EventEmitter class.
    t.ok( ent.events instanceof EventEmitter, "Extended component is a valid instance of the base EventEmitter." );
    
    
    
    // Add and test custom listeners.
    
    // Add and test plain listener.
    var plainListenerTest;
    var plainListener = function() {
        plainListenerTest = true;
    };
    ent.events.on( 'plainListener', plainListener );
    ent.events.emit( 'plainListener' );
    t.ok( plainListenerTest, "Plain listener working." );
    
    // Add and test listener that accesses context.
    var contextListenerTest;
    var contextListener = function() {
        contextListenerTest = this.ent;
    };
    ent.events.on( 'contextListener', contextListener );
    ent.events.emit( 'contextListener' );
    t.equal( ent, contextListenerTest, "Context-accessing listener working." );
    
    // Add and test listener that uses payloads.
    var payloadListenerTest;
    var payloadListener = function( a, b, c ) {
        payloadListenerTest = a+b+c;
    };
    ent.events.on( 'payloadListener', payloadListener );
    var a = 10;
    var b = 20;
    var c = 30;
    ent.events.emit( 'payloadListener', a, b, c );
    t.equal( a+b+c, payloadListenerTest, "Payload-accessing listener working." );
    
    // Add and test listener that accesses context and uses payload.
    var hybridListenerTest;
    var hybridListener = function( a, b, c ) {
        if ( contextListenerTest == this.ent && payloadListenerTest == a+b+c ) 
            hybridListenerTest = true;
    };
    ent.events.on( 'hybridListener', hybridListener );
    ent.events.emit( 'hybridListener', a, b, c );
    t.ok( hybridListenerTest, "Listener that accesses both the context and payloads working." );
    
    
    
    // Add and test internal events listeners.
    
    // Define listner addition listner.
    var newListenerTest;
    var newListener = function( event, listener ) {
        newListenerTest = { 
            event: event, 
            listener: listener 
        };
    };
    ent.events.on( 'newListener', newListener );
    
    // Define listener removal listener.
    var removeListenerTest;
    var removeListener = function( event, listener ) {
        removeListenerTest = { 
            event: event, 
            listener: listener 
        };
    };
    ent.events.on( 'removeListener', removeListener );
    console.log(newListenerTest);
    
    //Test addition of removeListener.
    t.ok( 
        newListenerTest.event === 'removeListener' && newListenerTest.listener === removeListener, 
        "Listerner that listens to addition of other listeners working." 
    );
    
    //Test removal newListener.
    ent.events.removeListener( 'newListener', newListener );
    t.ok( 
        removeListenerTest.event === 'newListener' && removeListenerTest.listener === newListener, 
        "Listerner that listens to removal of other listeners working." 
    );
    
    
    
    // Remove component.
    var _events = ent.events._events;
    ent.remove( events );
    t.equal( Object.getOwnPropertyNames(_events).length, 0, "Special method removing all listners upon removal of component working." );
    
    
    
    // Tests done.
    t.end();
    
});